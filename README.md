# Other stuff related to MP2I

## Math homework
In `dm_math` lies a math homework made for
someone else in my class.

[Yuesubi MP2I Other](https://gitlab.com/yuesubi-mp2i/other)
by [Yuesubi HEN](https://gitlab.com/yuesubi) is licensed under
[CC BY-NC-SA 4.0](http://creativecommons.org/licenses/by-nc-sa/4.0/?ref=chooser-v1)
