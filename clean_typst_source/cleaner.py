

import os


class FileSys:
    DIRECTORY = os.path.dirname(__file__)

    @staticmethod
    def read(file_path: str) -> str:
        with open(file_path, "r", encoding="utf-8") as file:
            return "".join(file.readlines())

    @staticmethod
    def write(file_path: str, content: str) -> None:
        with open(file_path, "w+", encoding="utf-8") as file:
            file.writelines(map(
                lambda line: line + "\n",
                content.split("\n")
            ))


if __name__ == "__main__":
    src = FileSys.read(os.path.join(FileSys.DIRECTORY, "samples/derivation.typ"))

    SUBSTITUTIONS = {
        "#cor": "#ycor(name: [])",
        "#definition": "#ydef(name: [])",
        "#ex": "#yexample",
        "#lemme": "#ylemme(name: [])",
        "#preuve": "#yproof",
        "#prop": "#yprop(name: [])",
        "#rem": "#ybtw",
        "#thm": "#ytheo(name: [])",
    }

    for (old, new) in SUBSTITUTIONS.items():
        src = src.replace(old, new)

    print(src)
