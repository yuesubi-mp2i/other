#import "@preview/cetz:0.2.0"


#set page(
    header: [#h(1fr) par Yuesubi HEN],
    numbering: "- 1 / 1 -"
)
#set enum(numbering: "1.a.")


// #let answer(content) = block(emph[#content])
#let answer(content) = {}


#align(center, text(size: 1.5em)[DM 14 et 15 -- Géométrie euclidienne élémentaire $star$$star$])


Dans ce DM, on s'interdira d'utiliser toute connaissance
préalable sur les angles.
Et donc, aussi les notions qui en découlent : trigonométrie,
forme exponencielle des complexes, _etc_...

= Définitions
+ Un _angle_ est composé de deux demi-droites dont
    les extrémités se joignent en un point.
+ Ce point est appellé _sommet de l'angle_.
+ On note $hat(A B C)$ l'angle de sommet $B$ et composé des deux
    demi-droites $[B A)$ et $[B C)$ (@angle).
+ On définit une relation d'équivalence $tilde$ pour les angles :

    $hat(A B C) tilde hat(D E F)$ si
    il existe une isométrie $I$ (une application bijective qui conserve
    les longueurs) telle que les points $A'$, $B'$ et $C'$, images des point
    $A$, $B$ et $C$ par $I$, forment un angle confondu avec $hat(D E F)$.
+ On munit l'ensemble des angles $cal(A)$ d'une loi de composition $xor$
    telle que
    - $xor$ est commutative ;
    - pour trois angles $alpha, beta, gamma$, on a
        $alpha tilde beta <==> alpha xor gamma tilde beta xor gamma$
    - pour $A, B, C$ trois points distincts et $M$ un point dans la portion
        de plan convexe délimité par les demi-droites $[B A)$ et $[B C)$,
        on a alors la relation de Chasles (@chasles) :
        $ hat(A B C) tilde hat(A B M) xor hat(M B C) $

    #align(center, table(
        rows: 1, columns: 2,
        stroke: none,
        [#figure(
            cetz.canvas(length: 4em, {
                import cetz.draw: *
     
                set-style(
                    mark: (fill: black),
                    stroke: (cap: "round"),
                    angle: (
                        radius: 1.7em,
                        label-radius: 3.4em,
                        fill: black.lighten(80%),
                        stroke: (paint: black.darken(50%))
                    ),
                    content: (padding: 0.4em)
                )
     
                let a_pos = (2.27*calc.cos(45deg), 2.27*calc.sin(45deg))
                let b_pos = (0, 0)
                let c_pos = (2, 0)
     
                cetz.angle.angle(b_pos, a_pos, c_pos, label: $hat(A B C)$)
     
                line(b_pos, a_pos)
                line(b_pos, c_pos)
     
                content((1, 1), $A$, anchor: "south-east")
                content(b_pos, $B$, anchor: "east")
                content((1.5, 0), $C$, anchor: "south")
            }),
            caption: [Exemple d'angle]
        ) <angle>],
        [#figure(
            cetz.canvas(length: 4em, {
                import cetz.draw: *
         
                set-style(
                    mark: (fill: black),
                    stroke: (cap: "round"),
                    angle: (
                        radius: 1.7em,
                        label-radius: 3.4em,
                        fill: black.lighten(80%),
                        stroke: (paint: black.darken(50%))
                    ),
                    content: (padding: 0.4em)
                )
     
                let a_pos = (0, 1.5)
                let b_pos = (0, 0)
                let c_pos = (2, 0)
                let m_pos = (2*calc.cos(30deg), 2*calc.sin(30deg))
     
                cetz.angle.angle(b_pos, a_pos, m_pos, label: $$)
                cetz.angle.angle(b_pos, m_pos, c_pos, label: $$)
     
                line(b_pos, a_pos)
                line(b_pos, c_pos)
                line(b_pos, m_pos)
     
                content((0, 2*calc.sin(45deg)), $A$, anchor: "east")
                content(b_pos, $B$, anchor: "east")
                content((2, 0), $C$, anchor: "south")
                content((m_pos.at(0)/2, m_pos.at(1)/2), $M$, anchor: "south-east")
            }),
            caption: [Relation de Chasles]
        ) <chasles>]
    ))

= Questions

+ Le but de cette question est des démontrer les propriétés
    de base sur les angles.
    
    + Montrer que les angles opposés par le sommet (@angles-by-summet)
        sont équivalents.
        #answer[Avec l'isométrie $z |-> -z$.]
    + Montrer que les angles correspondants, _i.e._ formés par une droite qui
        en coupe deux autres paralèlles (@corresponding-angles),
        sont équivalents.
        #answer[Avec une translation.]
    + Montrer qu'une triangle $A B C$ isocèle en $A$ vérifie
        $hat(A B C) tilde hat(B C A)$.

    #align(center, table(
        rows: 1, columns: 2,
        stroke: none,
        [#figure(
            cetz.canvas(length: 6em, {
                import cetz.draw: *
    
                set-style(
                    mark: (fill: black),
                    stroke: (cap: "round"),
                    angle: (
                        radius: 1.7em,
                        label-radius: 2.4em,
                        fill: black.lighten(80%),
                        stroke: (paint: black.darken(50%))
                    ),
                    content: (padding: 0.4em)
                )
    
                let tl_pos = (1, 0.5)
                let tr_pos = (-1, 0.5)
                let bl_pos = (1, -0.5)
                let br_pos = (-1, -0.5)
                let o_pos = (0, 0)
    
                cetz.angle.angle(o_pos, br_pos, tr_pos, label: $alpha$)
                cetz.angle.angle(o_pos, bl_pos, tl_pos, label: $beta$)
    
                line(tl_pos, br_pos)
                line(tr_pos, bl_pos)
    
                content(o_pos, $S$, anchor: "south")
            }),
            caption: [Angles opposés par le sommet]
        ) <angles-by-summet>],
        [#figure(
            cetz.canvas(length: 6em, {
                import cetz.draw: *
    
                set-style(
                    mark: (fill: black),
                    stroke: (cap: "round"),
                    angle: (
                        radius: 1.7em,
                        label-radius: 2.4em,
                        fill: black.lighten(80%),
                        stroke: (paint: black.darken(50%))
                    ),
                    content: (padding: 0.4em)
                )
    
                let h = 0.4
                let hp = 0.2
                let d = 0.5
    
                let o_pos = (0, 0)
                let i_pos = ((h - hp) * d / (h + hp), h - hp)
                let j_pos = (-h * d / (h+hp), -h)
    
                cetz.angle.angle(i_pos, (1, h - hp), (d, h + hp), label: $alpha$)
                cetz.angle.angle(j_pos, (0, -h), o_pos, label: $beta$)
    
                line((-1, h - hp), (1, h - hp))
                line((-1, -h), (1, -h))
                line((-d, -h - hp), (d, h + hp))
    
                content((-d, h), $(Delta)$, anchor: "north")
                content((d, -h), $(Delta')$, anchor: "south")
            }),
            caption: [Angles correspondants]
        ) <corresponding-angles>]
    ))

// + On dit que deux angles $alpha$ et $beta$
//     sont alternes-internes s'ils sont
//     formés par les intersection d'une droite $(T)$
//     avec deux droites $(Delta)$ et $(Delta')$ paralèlles
//     non confondues dans la configuration de la
//     @angles-alternes-internes.
// 
//     #figure(
//         cetz.canvas(length: 6em, {
//             import cetz.draw: *
// 
//             set-style(
//                 mark: (fill: black),
//                 stroke: (cap: "round"),
//                 angle: (
//                     radius: 1.7em,
//                     label-radius: 2.4em,
//                     fill: black.lighten(80%),
//                     stroke: (paint: black.darken(50%))
//                 ),
//                 content: (padding: 0.4em)
//             )
// 
//             let h = 0.4
//             let hp = 0.2
//             let d = 0.5
// 
//             let o_pos = (0, 0)
//             let i_pos = (h * d / (h+hp), h)
//             let j_pos = (-h * d / (h+hp), -h)
// 
//             cetz.angle.angle(i_pos, (0, h), o_pos, label: $alpha$)
//             cetz.angle.angle(j_pos, (0, -h), o_pos, label: $beta$)
// 
//             line((-1, h), (1, h))
//             line((-1, -h), (1, -h))
//             line((-d, -h - hp), (d, h + hp))
// 
//             content((-d, h), $(Delta)$, anchor: "north")
//             content((d, -h), $(Delta')$, anchor: "south")
//         }),
//         caption: [Angles alternes-internes]
//     ) <angles-alternes-internes>
// 
//     Montrer que deux angles alternes-internes sont équivalents.
// 
//     #answer[Avec un angle opposé par le sommet et un angle correspondant.]

+ À partir de cette question, on n'utilisera plus les isométries.

    On note $pi$ l'angle entre les deux demi-droites d'une
    droite séparées par un point (@pi-angle).

    #figure(
        cetz.canvas(length: 6em, {
            import cetz.draw: *

            set-style(
                mark: (fill: black),
                stroke: (cap: "round"),
                angle: (
                    radius: 1.7em,
                    label-radius: 2.4em,
                    fill: black.lighten(80%),
                    stroke: (paint: black.darken(50%))
                ),
                content: (padding: 0.4em)
            )

            let o_pos = (0, 0)
            let a_pos = (-0.75, 0)
            let b_pos = (0.75, 0)

            cetz.angle.angle(o_pos, a_pos, b_pos, label: $pi$)

            line((-1, 0), (1, 0))

            line((o_pos.at(0), -0.03), (o_pos.at(0), 0.03))
            line((a_pos.at(0), -0.03), (a_pos.at(0), 0.03))
            line((b_pos.at(0), -0.03), (b_pos.at(0), 0.03))

            content(o_pos, $O$, anchor: "north")
            content(a_pos, $A$, anchor: "north")
            content(b_pos, $B$, anchor: "north")

        }),
        caption: [Angle $pi$]
    ) <pi-angle>

    Montrer que la somme des trois angles d'un triangle
    vaut $pi$.

+ 
    - Un angle est dit inscrit à un cercle $Gamma$ si il est formé par
        trois points qui appartiennent à $Gamma$ (@inscribled-angle) ;
    - L'arc de cet angle est la portion du cercle entre les deux demi-droites
        (trait épais sur @inscribled-angle et @center-angle) ;
    - Un angle est dit au centre s'il est formé de deux points sur $Gamma$
        et de son centre $O$, avec $O$ qui est le sommet de l'angle
        (@center-angle).

    #align(center, table(
        rows: 1, columns: 3,
        stroke: none,
        [#figure(
            cetz.canvas(length: 5em, {
                import cetz.draw: *
    
                set-style(
                    mark: (fill: black),
                    stroke: (cap: "round"),
                    angle: (
                        radius: 1.7em,
                        label-radius: 2.4em,
                        fill: black.lighten(80%),
                        stroke: (paint: black.darken(50%))
                    ),
                    content: (padding: 0.3em)
                )
    
    
                let o_pos = (0, 0)
                let a_pos = (name: "circle", anchor: -160deg)
                let b_pos = (name: "circle", anchor: 70deg)
                let c_pos = (name: "circle", anchor: -10deg)
    
                circle(radius: 1.0, o_pos, name: "circle")
    
                content((0, 0), $O$, anchor: "south-east")
                content(a_pos, $A$, anchor: "north-east")
                content(b_pos, $B$, anchor: "south-west")
                content(c_pos, $C$, anchor: "west")
    
                cetz.angle.angle(a_pos, b_pos, c_pos, label: $alpha$)
    
                arc(b_pos, start: 70deg, stop: -10deg, stroke: (thickness: 0.2em))
    
                line(a_pos, b_pos)
                line(a_pos, c_pos)

                line((-0.03, 0), (0.03, 0))
                line((0, -0.03), (0, 0.03))
            }),
            caption: [Angle inscrit]
        ) <inscribled-angle>],
        [#figure(
            cetz.canvas(length: 5em, {
                import cetz.draw: *
    
                set-style(
                    mark: (fill: black),
                    stroke: (cap: "round"),
                    angle: (
                        radius: 1.7em,
                        label-radius: 2.4em,
                        fill: black.lighten(80%),
                        stroke: (paint: black.darken(50%))
                    ),
                    content: (padding: 0.3em)
                )
    
    
                let o_pos = (0, 0)
                let a_pos = (name: "circle", anchor: -160deg)
                let b_pos = (name: "circle", anchor: 70deg)
                let c_pos = (name: "circle", anchor: -10deg)
    
                circle(radius: 1.0, o_pos, name: "circle")
    
                content((0, 0), $O$, anchor: "south-east")
                content(b_pos, $B$, anchor: "south-west")
                content(c_pos, $C$, anchor: "west")
    
                cetz.angle.angle(o_pos, b_pos, c_pos, label: $beta$)
    
                arc(b_pos, start: 70deg, stop: -10deg, stroke: (thickness: 0.2em))
    
                line(o_pos, b_pos)
                line(o_pos, c_pos)
            }),
            caption: [Angle au centre]
        ) <center-angle>],
        [#figure(
            cetz.canvas(length: 5em, {
                import cetz.draw: *
    
                set-style(
                    mark: (fill: black),
                    stroke: (cap: "round"),
                    angle: (
                        radius: 1.7em,
                        label-radius: 2.4em,
                        fill: black.lighten(80%),
                        stroke: (paint: black.darken(50%))
                    ),
                    content: (padding: 0.3em)
                )
    
    
                let o_pos = (0, 0)
                let a_pos = (name: "circle", anchor: -160deg)
                let b_pos = (name: "circle", anchor: 70deg)
                let c_pos = (name: "circle", anchor: -10deg)
    
                circle(radius: 1.0, o_pos, name: "circle")
    
                content((0, 0), $O$, anchor: "south-east")
                content(a_pos, $A$, anchor: "north-east")
                content(b_pos, $B$, anchor: "south-west")
                content(c_pos, $C$, anchor: "west")
    
                cetz.angle.angle(o_pos, b_pos, c_pos, label: $2 alpha$)
                cetz.angle.angle(a_pos, b_pos, c_pos, label: $alpha$)
    
                arc(b_pos, start: 70deg, stop: -10deg, stroke: (thickness: 0.2em))
    
                line(a_pos, b_pos)
                line(a_pos, c_pos)
                line(o_pos, b_pos)
                line(o_pos, c_pos)
            }),
            caption: [Théorème de l'angle au centre]
        ) <center-angle-theorem>]
    ))

    Le but de cette question est de prouver le théorème
    de l'angle au centre (@center-angle-theorem) :
    "L'angle au centre qui intercepte le même arc
    qu'un angle inscrit est équivalent au double de celui-ci."

    + Montrer le théorème dans le cas où $O in (A C)$.
    + Montrer le théorème dans les autres cas.

+ En déduire le théorème de l'angle inscrit :
    "Tous les angles inscrits interceptant le même arc
    de cercle sont équivalents."

+ Un angle tangenciel est formé d'une tangente à un cercle (donc fait un angle
    avec $[O A]$ dont le double équivaut à $pi$) et de deux points sur le cercle
    dans la configuration de la @tangencial-angle.

    Montrer l'angle au centre qui intercepte le même arc
    qu'un angle tangenciel est équivalent au double de celui-ci
    (@tangencial-angle-center).

    #align(center, table(
        rows: 1, columns: 2,
        stroke: none,
        [#figure(
            cetz.canvas(length: 5em, {
                import cetz.draw: *
    
                set-style(
                    mark: (fill: black),
                    stroke: (cap: "round"),
                    angle: (
                        radius: 1.7em,
                        label-radius: 2.3em,
                        fill: black.lighten(80%),
                        stroke: (paint: black.darken(50%))
                    ),
                    content: (padding: 0.3em)
                )
    
    
                let o_pos = (0, 0)
                let a_pos = (name: "circle", anchor: -160deg)
                let a_pos = (name: "circle", anchor: 135deg)
                let b_pos = (name: "circle", anchor: 25deg)
    
                circle(radius: 1.0, o_pos, name: "circle")
    
                content((0, 0), $O$, anchor: "north-east")
                content(a_pos, $A$, anchor: "south-east")
                content(b_pos, $B$, anchor: "south-west")
    
                cetz.angle.angle(a_pos,
                    (calc.cos(135deg) + 1, calc.sin(135deg) + 1),
                    b_pos, label: $alpha$)
    
                arc(a_pos, start: 135deg, stop: 25deg, stroke: (thickness: 0.2em))
    
                line(a_pos, b_pos)

                line((-0.75 + calc.cos(135deg), -0.75 + calc.sin(135deg)),
                    (0.75 + calc.cos(135deg), 0.75 + calc.sin(135deg)))

                line((-0.03, 0), (0.03, 0))
                line((0, -0.03), (0, 0.03))
            }),
            caption: [Angle tangenciel]
        ) <tangencial-angle>],
        [#figure(
            cetz.canvas(length: 5em, {
                import cetz.draw: *
    
                set-style(
                    mark: (fill: black),
                    stroke: (cap: "round"),
                    angle: (
                        radius: 1.7em,
                        label-radius: 2.3em,
                        fill: black.lighten(80%),
                        stroke: (paint: black.darken(50%))
                    ),
                    content: (padding: 0.3em)
                )
    
    
                let o_pos = (0, 0)
                let a_pos = (name: "circle", anchor: -160deg)
                let a_pos = (name: "circle", anchor: 135deg)
                let b_pos = (name: "circle", anchor: 25deg)
    
                circle(radius: 1.0, o_pos, name: "circle")
    
                content((0, 0), $O$, anchor: "north-east")
                content(a_pos, $A$, anchor: "south-east")
                content(b_pos, $B$, anchor: "south-west")
    
                cetz.angle.angle(o_pos, a_pos, b_pos, label: $2 alpha$)
                cetz.angle.angle(a_pos,
                    (calc.cos(135deg) + 1, calc.sin(135deg) + 1),
                    b_pos, label: $alpha$)
    
                arc(a_pos, start: 135deg, stop: 25deg, stroke: (thickness: 0.2em))
    
                line(a_pos, b_pos)
                line(o_pos, a_pos)
                line(o_pos, b_pos)

                line((-0.75 + calc.cos(135deg), -0.75 + calc.sin(135deg)),
                    (0.75 + calc.cos(135deg), 0.75 + calc.sin(135deg)))
            }),
            caption: [L'angle au centre est deux fois celui tangenciel]
        ) <tangencial-angle-center>]
    ))
